package cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Domain container contains the List of Domains
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public abstract class DomainContainer {
    protected ArrayList<Domain> domains;
    protected String directory;
    protected String backupPrefix;
    protected String backupAllDomains;
    protected String backupDomainsBounds;
    protected String arffDir;
    protected String arffPredicates;
    protected String arffTypes;
    protected String arffSubjects;
    
    /**
     * Constructor.
     */
    public DomainContainer() {
        domains = new ArrayList<>();
    }
    
    /**
     * Add domain.
     * @param d domain
     */
    public void addDomain(Domain d) {
        domains.add(d);
    }
    
    /**
     * @return domains
     */
    public ArrayList<Domain> getDomains() {
        return domains;
    }
    
    /**
     * @return domainNames
     */
    public ArrayList<String> getDomainNames() {
        ArrayList<String> res = new ArrayList<>();
        for (Domain d : domains) { res.add(d.getName()); }
        return res;
    }
    
    /**
     * Printing a part of ARFF dataset for predicates to file.
     */
    public void printArffPredicatesDataset() {
        ArrayList<String> mergedAnalyzedPredicates = new ArrayList<>();
        PrintWriter writer;
        try {
            File dir = new File("data/"+arffDir+"/"+directory);
            if (!dir.exists()) { dir.mkdirs(); }
            writer = new PrintWriter("data/"+arffDir+"/"+directory+"/"+arffPredicates+".arff", "UTF-8");
            writer.println("@RELATION LearningDomainsForNamedEntities_"+directory+"_predicates\n");

            for (Domain d : domains) {
                for (Domain.MyInfo mi : d.getAnalyzedPredicates()) {
                    if (!mergedAnalyzedPredicates.contains(mi.name)) {
                        mergedAnalyzedPredicates.add(mi.name);
                    }
                }
            }
            for (String predicate : mergedAnalyzedPredicates) {
                //System.out.println("@ATTRIBUTE '"+predicate+"' {f,t}");
                writer.println("@ATTRIBUTE '"+predicate+"' {f,t}");
            }

            //System.out.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            //System.out.println("@DATA");
            writer.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            writer.println("@DATA");
            for (Domain d : domains) {
                for (Entity entity : d.getEntities()) {
                    for (String predicate : mergedAnalyzedPredicates) {
                        if (entity.getPredicates().contains(predicate)) {
                            //System.out.print("t,");
                            writer.print("t,");
                        }
                        else {
                            //System.out.print("f,");
                            writer.print("f,");
                        }
                    }
                    //System.out.print(d.getName()+"\n");
                    writer.print(d.getName()+"\n");
                }
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Printing a part of ARFF dataset for rdf:types to file.
     */
    public void printArffTypesDataset() {
        ArrayList<String> mergedAnalyzedTypes = new ArrayList<>();
        PrintWriter writer;
        try {
            File dir = new File("data/"+arffDir+"/"+directory);
            if (!dir.exists()) { dir.mkdirs(); }
            writer = new PrintWriter("data/"+arffDir+"/"+directory+"/"+arffTypes+".arff", "UTF-8");
            writer.println("@RELATION LearningDomainsForNamedEntities_"+directory+"_types\n");
            for (Domain d : domains) {
                for (Domain.MyInfo mi : d.getAnalyzedTypes()) {
                    if (!mergedAnalyzedTypes.contains(mi.name)) {
                        mergedAnalyzedTypes.add(mi.name);
                    }
                }
            }
            for (String type : mergedAnalyzedTypes) {
                //System.out.println("@ATTRIBUTE '"+type+"' {f,t}");
                writer.println("@ATTRIBUTE '"+type+"' {f,t}");
            }

            //System.out.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            //System.out.println("@DATA");
            writer.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            writer.println("@DATA");
            for (Domain d : domains) {
                for (Entity entity : d.getEntities()) {
                    for (String type : mergedAnalyzedTypes) {
                        if (entity.getTypes().contains(type)) {
                            //System.out.print("t,");
                            writer.print("t,");
                        }
                        else {
                            //System.out.print("f,");
                            writer.print("f,");
                        }
                    }
                    //System.out.print(d.getName()+"\n");
                    writer.print(d.getName()+"\n");
                }
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Printing a part of ARFF dataset for dcterms:subjects to file.
     */
    public void printArffSubjectsDataset() {
        ArrayList<String> mergedAnalyzedSubjects = new ArrayList<>();
        PrintWriter writer;

        try {
            File dir = new File("data/"+arffDir+"/"+directory);
            if (!dir.exists()) { dir.mkdirs(); }
            writer = new PrintWriter("data/"+arffDir+"/"+directory+"/"+arffSubjects+".arff", "UTF-8");
            writer.println("@RELATION LearningDomainsForNamedEntities_"+directory+"_subjects\n");

            for (Domain d : domains) {
                for (Domain.MyInfo mi : d.getAnalyzedSubjects()) {
                    if (!mergedAnalyzedSubjects.contains(mi.name)) {
                        mergedAnalyzedSubjects.add(mi.name);
                    }
                }
            }
            for (String subject : mergedAnalyzedSubjects) {
                //System.out.println("@ATTRIBUTE '"+subject+"' {f,t}");
                writer.println("@ATTRIBUTE '"+subject+"' {f,t}");
            }

            //System.out.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            //System.out.println("@DATA");
            writer.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            writer.println("@DATA");
            for (Domain d : domains) {
                for (Entity entity : d.getEntities()) {
                    for (String subject : mergedAnalyzedSubjects) {
                        if (entity.getSubjects().contains(subject)) {
                            //System.out.print("t,");
                            writer.print("t,");
                        }
                        else {
                            //System.out.print("f,");
                            writer.print("f,");
                        }
                    }
                    //System.out.print(d.getName()+"\n");
                    writer.print(d.getName()+"\n");
                }
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Printing a part of ARFF dataset for all info to file.
     */
    public void printArffAllDataset() {
        ArrayList<String> mergedAnalyzedInfo = new ArrayList<>();
        PrintWriter writer;
        try {
            File dir = new File("data/"+arffDir+"/"+directory);
            if (!dir.exists()) { dir.mkdirs(); }
            writer = new PrintWriter("data/"+arffDir+"/"+directory+"/all_info.arff", "UTF-8");
            writer.println("@RELATION LearningDomainsForNamedEntities_"+directory+"_all_info\n");

            for (Domain d : domains) {
                for (Domain.MyInfo mi : d.getAnalyzedPredicates()) {
                    if (!mergedAnalyzedInfo.contains(mi.name)) {
                        mergedAnalyzedInfo.add(mi.name);
                    }
                }
                for (Domain.MyInfo mi : d.getAnalyzedTypes()) {
                    if (!mergedAnalyzedInfo.contains(mi.name)) {
                        mergedAnalyzedInfo.add(mi.name);
                    }
                }
                for (Domain.MyInfo mi : d.getAnalyzedSubjects()) {
                    if (!mergedAnalyzedInfo.contains(mi.name)) {
                        mergedAnalyzedInfo.add(mi.name);
                    }
                }
            }
            for (String info : mergedAnalyzedInfo) {
                //System.out.println("@ATTRIBUTE '"+predicate+"' {f,t}");
                writer.println("@ATTRIBUTE '"+info+"' {f,t}");
            }

            //System.out.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            //System.out.println("@DATA");
            writer.println("@ATTRIBUTE class {"+String.join(",",getDomainNames())+"}");
            writer.println("@DATA");
            for (Domain d : domains) {
                for (Entity entity : d.getEntities()) {
                    for (String info : mergedAnalyzedInfo) {
                        if (entity.getPredicates().contains(info)
                            || entity.getTypes().contains(info)
                            || entity.getSubjects().contains(info)) {
                            //System.out.print("t,");
                            writer.print("t,");
                        }
                        else {
                            //System.out.print("f,");
                            writer.print("f,");
                        }
                    }
                    //System.out.print(d.getName()+"\n");
                    writer.print(d.getName()+"\n");
                }
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Save JSON file with the list of domains and their entities.
     * Only for analysis.
     */
    public void saveDomainsWithEntities() {
        try {
            File dir = new File("data/analysis/"+directory);
            if (!dir.exists()) { dir.mkdirs(); }
            try (PrintWriter writer = new PrintWriter(dir+"/domains_and_entities.json", "UTF-8")) {
                JSONObject json = new JSONObject();
                for (Domain d : domains) {
                    JSONObject domain = new JSONObject();
                    domain.put("entities", d.getEntitiesUrls());
                    json.put(d.getName(), domain);
                }
                json.put("domains", getDomainNames());
                writer.println(json.toJSONString());
            }
        } catch (IOException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    /**
     * Saves all domains to JSON file.
     * Path to file is set in config file.
     */
    public void backupDomains() {
        try {
            File dir = new File("data/backup/"+directory);
            if (!dir.exists()) { dir.mkdirs(); }
            try (PrintWriter writer = new PrintWriter("data/backup/"+directory+"/"+backupAllDomains+".json", "UTF-8")) {
                int total=0, min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
                JSONObject json = new JSONObject();
                for (Domain d : domains) {
                    int size = d.getEntities().size();
                    total += size;
                    if (size < min) { min = size; }
                    if (size > max) { max = size; }
                    JSONObject domain = new JSONObject();
                    domain.put("entitiesCount", size);
                    json.put(d.getName(), domain);
                }
                json.put("maxCountEntities", max);
                json.put("minCountEntities", min);
                json.put("averageCountEntities", total / domains.size());
                json.put("domains", getDomainNames());
                writer.println(json.toJSONString());
            }
        } catch (IOException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Saves domain to JSON file.
     * Path to file is set in config file.
     * @param buDomain Domain
     */
    public void backupDomain(Domain buDomain) {
        try {
            File dir = new File("data/backup/"+directory);
            if (!dir.exists()) { dir.mkdirs(); }
            try (PrintWriter writer = new PrintWriter("data/backup/"+directory+"/"+backupPrefix+buDomain.getName()+".json", "UTF-8")) {
                JSONObject json = new JSONObject();
                
                JSONObject entities = new JSONObject();
                for (Entity e : buDomain.getEntities()) {
                    JSONObject entity = new JSONObject();
                    entity.put("predicates", e.getPredicates());
                    entity.put("rdf:types", e.getTypes());
                    entity.put("dcterms:subjects", e.getSubjects());
                    entities.put(e.getUrl(), entity);
                }
                json.put("name", buDomain.getName());
                json.put("upperBound", buDomain.getPredicateUpperBound());
                json.put("lowerBound", buDomain.getPredicateLowerBound());
                json.put("entities", entities);
                writer.println(json.toJSONString());
            }
        } catch (IOException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets all domains from backuped JSON file.
     * Path to file is set in config file.
     * @return list of domains
     */
    public ArrayList<String> getBackupDomains() {
        JSONParser parser = new JSONParser();
        ArrayList<String> res = null;
        try {
            JSONObject obj = (JSONObject) parser.parse(new FileReader("data/backup/"+directory+"/"+backupAllDomains+".json"));
            res = new ArrayList<>((JSONArray)obj.get("domains"));
        } catch (IOException | ParseException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    /**
     * Checks if JSON file of domain exists.
     * Path to file is set in config file.
     * @param domain
     * @return true / false
     */
    public boolean domainBackupExists(String domain) {
        File file = new File("data/backup/"+directory+"/"+backupPrefix+domain+".json");
        if (file.exists() && !file.isDirectory()) {
            return true;
        }
        return false;
    }
    
    /**
     * Getting upper bound of domain from JSON file.
     * JSON file was created manually.
     * Path to file is set in config file.
     * @param domain
     * @return double value of upper bound
     */
    public double getDomainBackupUpperBound(String domain) {
        JSONParser parser = new JSONParser();
        double res = Double.NaN;
        try {
            JSONObject obj = (JSONObject) parser.parse(new FileReader("data/backup/"+directory+"/"+backupDomainsBounds+".json"));
            JSONObject d = (JSONObject) obj.get(domain);
            res = (double) d.get("upperBound");
        } catch (IOException | ParseException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    /**
     * Getting lower bound of domain from JSON file.
     * JSON file was created manually.
     * Path to file is set in config file.
     * @param domain
     * @return double value of upper bound
     */
    public double getDomainBackupLowerBound(String domain) {
        JSONParser parser = new JSONParser();
        double res = Double.NaN;
        try {
            JSONObject obj = (JSONObject) parser.parse(new FileReader("data/backup/"+directory+"/"+backupDomainsBounds+".json"));
            JSONObject d = (JSONObject) obj.get(domain);
            res = (double) d.get("lowerBound");
        } catch (IOException | ParseException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    /**
     * Restores domain from JSON file.
     * Path to file is set in config file.
     * @param resDomain 
     */
    public void restoreDomain(String resDomain) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse(new FileReader("data/backup/"+directory+"/"+backupPrefix+resDomain+".json"));
            Domain newDomain = new Domain((String) obj.get("name"));
            newDomain.setPredicateUpperBound((double) obj.get("upperBound"));
            newDomain.setPredicateLowerBound((double) obj.get("lowerBound"));
            JSONObject entities = (JSONObject) obj.get("entities");
            for (Object e : entities.keySet()) {
                Entity newEntity = new Entity(e.toString());
                JSONObject entity = (JSONObject) entities.get(e.toString());
                JSONArray predicates = (JSONArray) entity.get("predicates");
                JSONArray types = (JSONArray) entity.get("rdf:types");
                JSONArray subjs = (JSONArray) entity.get("dcterms:subjects");
                newEntity.setPredicates(predicates);
                newEntity.setTypes(types);
                newEntity.setSubjects(subjs);
                newDomain.addEntity(newEntity);
            }
            domains.add(newDomain);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(DomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
