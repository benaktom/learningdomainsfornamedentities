package cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.Domain;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;

/**
 *
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class DomainExtension {

    public void addSimilarEntitiesByTypes(Domain domain) {
        /// getting groups of rdf:types from existing entities 
        ArrayList<String> types = new ArrayList<>();
        for (Entity e : domain.getEntities()) {
            String t = "";
            for (int i=0; i<e.getTypes().size(); i++) {
                String s = e.getTypes().get(i).trim();
                t += "<"+s+">";
                if (i < e.getTypes().size() - 1) {
                    t += ",";
                }
            }
            if (!types.contains(t) && !t.isEmpty()) {
                types.add(t);
            }
        }
        
        /// adding new entities to domain
        try {
            HDT hdt = HDTManager.mapIndexedHDT("/home/tomas/FIT/MI/DIP/dbpedia2015en.hdt", null);
            
            // Create Jena Model on top of HDT.
            HDTGraph graph = new HDTGraph(hdt);
            Model model = ModelFactory.createModelForGraph(graph);
            
            for (String s : types) {
                String queryString1 = "" +
                        "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
                        "SELECT DISTINCT ?subject " +
                        "where { " +
                        "?subject rdf:type "+ s +" . }";
                QueryExecution qexec = QueryExecutionFactory.create(queryString1, model);
                try {
                    ResultSet rs = qexec.execSelect();
                    while (rs.hasNext() && domain.getEntities().size() < 30) {
                        QuerySolution rb = rs.nextSolution() ;
                        RDFNode x = rb.get("subject") ;
                        if (!domain.getEntitiesUrls().contains(x.toString())) {
                            Entity e = new Entity(x.toString());
                            e.setInformations();
                            System.out.print(".");
                            domain.addEntity(e);
                        }
                    }
                }
                finally {
                    qexec.close() ;
                }
            }
        }
        catch (IOException ex) {
            Logger.getLogger(DomainExtension.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
