package cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.Domain;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Implementation of InformationStrategy for rdf:types
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class TypesStrategy implements InformationStrategy {

    /**
     * @param domain 
     */
    @Override
    public void analyzeInformations(Domain domain) {
        System.out.print("Analyzing rdf:types for "+domain.getName()+"...");
        HashMap res;
        int entitesCount = 0;
        res = new HashMap<String, Integer>();
        for (Entity e : domain.getEntities()) {
            for (String s : e.getTypes()) {
                if (res.containsKey(s)) {
                    Integer i = (Integer)res.get(s);
                    res.replace(s, i+1);
                }
                else {
                    res.put(s, 1);
                }
            }
            entitesCount++;
        }
        double perc;
        Iterator<String> it = res.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            Object val = res.get(key);
            perc = (int)val / (double)entitesCount;
            /// ATTENTION! trimming insignificant informations
            if (key.contains("http://dbpedia.org/ontology/")) {
                domain.addAnalyzedTypes(new Domain.MyInfo(key, perc));
            }
        }
        System.out.print("ok\n");
    }

}
