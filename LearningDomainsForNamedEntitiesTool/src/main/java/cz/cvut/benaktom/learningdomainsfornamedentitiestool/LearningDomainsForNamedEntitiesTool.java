package cz.cvut.benaktom.learningdomainsfornamedentitiestool;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.Domain;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.FADomainContainer;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.MyDomainContainer;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.GetFAIds;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy.PredicatesStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy.SubjectsStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy.TypesStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.DomainExtension;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy.EntityPredicatesStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy.EntitySubjectsStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy.EntityTypesStrategy;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class LearningDomainsForNamedEntitiesTool {
    
    private static Properties prop;
    
    /**
     * Constructor.
     * Load config file
     */
    public LearningDomainsForNamedEntitiesTool() {
        prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file 'config.properties' not found in the classpath");
            }
        } catch (IOException ex) {
            Logger.getLogger(LearningDomainsForNamedEntitiesTool.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Main function.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LearningDomainsForNamedEntitiesTool l = new LearningDomainsForNamedEntitiesTool();

        /**
         * reading args
         * 1 -> my entities, 2 - featured articles entites, 3 - get FA ids
         */
        int firstArg = 0;
        int secondArg = 0;

        if (args.length != 2) {
            Logger.getLogger(LearningDomainsForNamedEntitiesTool.class.getName()).log(Level.SEVERE, "Exactly 2 parameters required.", new IllegalArgumentException());
            System.exit(0);
        }
        try {
            firstArg = Integer.parseInt(args[0]);
            secondArg = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            Logger.getLogger(LearningDomainsForNamedEntitiesTool.class.getName()).log(Level.SEVERE, "Both args must be an integer.", ex);
            System.exit(0);
        }
        
        String path;
        File dir;
        
        switch (firstArg) {
            /**
             * MyEntities - three "hand-made" domains with entities
             */
            case 1:
                System.out.println("=== Loading My entities ===");
                processMyDomains(secondArg);
                break;
            /** 
             * Featured Articles - all domains and its entities from 
             * https://en.wikipedia.org/wiki/Wikipedia:Featured_articles
             */
            case 2:
                System.out.println("=== Featured Articles entites ===");
                processFADomains(secondArg);
                break;
            /** 
             * Getting all entities from https://en.wikipedia.org/wiki/Wikipedia:Featured_articles
             * and analyze its representation for its domain
             * (helper for csv tables and long tail problem)
             */
            case 3:
                /// getting domains from webpage
                GetFAIds ids = new GetFAIds();
                FADomainContainer fadc = new FADomainContainer();

                /// adds domais to domain container and sets its entities
                for (String faDomain : ids.getIds()) {
                    faDomain = faDomain.replace("#", "");
                    System.out.println(faDomain);
                    fadc.addEntities(faDomain, faDomain, null, null);
                }

                /// setting and analyzing predicates
                for (Domain d : fadc.getDomains()) {
                    for (Entity e : d.getEntities()) {
                        e.setInformations(new EntityPredicatesStrategy());
                        System.out.println(e.getUrl()+": p="+e.getPredicates().size());
                    }
                    d.analyze(new PredicatesStrategy());
                }

                /// writing to text file
                path = "data/" + prop.getProperty("FAAnalyzedPredicatesDir");
                dir = new File(path);
                if (!dir.exists()) { dir.mkdirs(); }
                for (Domain d : fadc.getDomains()) {
                    try (PrintWriter writer = new PrintWriter(path+"/" + d.getName()+".txt", "UTF-8")) {
                        for (Domain.MyInfo p : d.getAnalyzedPredicates()) {
                            writer.println(p.name + " " + p.percent);
                            System.out.println(p.name + " " + p.percent);
                        }
                        writer.close();
                    } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                        Logger.getLogger(LearningDomainsForNamedEntitiesTool.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
            default:
                System.out.println("unsupported type of informations");
                break;
        }
    }

    /**
     * MyEntities - three manually created domains with entities
     * @param typeOfInfo
     */
    private static void processMyDomains(int typeOfInfo) {
        MyDomainContainer mdc = new MyDomainContainer();

        switch (typeOfInfo) {
            case 1: // predicates
                mdc.addMyEntities();
                for (Domain d : mdc.getDomains()) {
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) {
                        e.setInformations(new EntityPredicatesStrategy());
                        System.out.print(".");
                    }
                    System.out.println(".");
                    d.analyze(new PredicatesStrategy());
                }
                mdc.printArffPredicatesDataset();
                break;
            case 2: // rdf:types
                mdc.addMyEntities();
                for (Domain d : mdc.getDomains()) {
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) {
                        e.setInformations(new EntityTypesStrategy());
                        System.out.print(".");
                    }
                    System.out.println(".");
                    d.analyze(new TypesStrategy());
                }
                mdc.printArffTypesDataset();
                break;
            case 3: // dcterms:subjects
                mdc.addMyEntities();
                for (Domain d : mdc.getDomains()) {
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) {
                        e.setInformations(new EntitySubjectsStrategy());
                        System.out.print(".");
                    }
                    System.out.println(".");
                    d.analyze(new SubjectsStrategy());
                }
                mdc.printArffSubjectsDataset();
                break;
            case 4: // all - with backup
                mdc.addMyEntities();
                for (Domain d : mdc.getDomains()) {
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) {
                        e.setInformations();
                        System.out.print(".");
                    }
                    System.out.println(".");
                    mdc.backupDomain(d);
                    d.analyze();
                }
                mdc.backupDomains();
                // OPTIONAL - only for analysis
                //mdc.saveDomainsWithEntities();
                mdc.printArffPredicatesDataset();
                mdc.printArffTypesDataset();
                mdc.printArffSubjectsDataset();
                break;
            default:
                System.out.println("unsupported type of informations");
                break;
        }
    }
    
    /**
     * Featured Articles - all domains and its entities from 
     * https://en.wikipedia.org/wiki/Wikipedia:Featured_articles
     * @param typeOfInfo
     */
    private static void processFADomains(int typeOfInfo) {
        FADomainContainer fadc = new FADomainContainer();

        switch (typeOfInfo) {
            case 1: // predicates
                fadc.addFAEntities();
                for (Domain d : fadc.getDomains()) {
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) {
                        e.setInformations(new EntityPredicatesStrategy());
                        System.out.print(".");
                    }
                    System.out.println(".");
                    d.analyze(new PredicatesStrategy());
                }
                fadc.printArffPredicatesDataset();
                break;
            case 2: // rdf:types
                fadc.addFAEntities();
                for (Domain d : fadc.getDomains()) {
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) {
                        e.setInformations(new EntityTypesStrategy());
                        System.out.print(".");
                    }
                    System.out.println(".");
                    d.analyze(new TypesStrategy());
                }
                fadc.printArffTypesDataset();
                break;
            case 3: // dcterms:subjects
                fadc.addFAEntities();
                for (Domain d : fadc.getDomains()) {
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) {
                        e.setInformations(new EntitySubjectsStrategy());
                        System.out.print(".");
                    }
                    System.out.println(".");
                    d.analyze(new SubjectsStrategy());
                }
                fadc.printArffSubjectsDataset();
                break;
            case 4: // all - only for backup
                fadc.addFAEntities();
                for (Domain d : fadc.getDomains()) { // for all domains
                    System.out.print(d.getName());
                    for (Entity e : d.getEntities()) { // for all entities of domains
                        e.setInformations();
                        System.out.print(".");
                    }
                    System.out.println(".");
                    fadc.backupDomain(d);
                    d.analyze();
                }
                fadc.backupDomains();
                break;
            case 5: // all - restoring domains and printing arff
                for (String domain : fadc.getBackupDomains()) {
                    if (fadc.domainBackupExists(domain)) {
                        fadc.restoreDomain(domain);
                    }
                    else {
                        System.out.println(domain + " - domain backup does not exist.");
                        Domain newDomain = fadc.addEntities(domain, domain, fadc.getDomainBackupUpperBound(domain), fadc.getDomainBackupLowerBound(domain));
                        System.out.print(newDomain.getName());
                        for (Entity e : newDomain.getEntities()) { // for all entities of domains
                            e.setInformations();
                            System.out.print(".");
                        }
                        System.out.println(".");
                        fadc.backupDomain(newDomain);
                    }
                }
                for (Domain d : fadc.getDomains()) { 
                    d.analyze();
                }
                fadc.backupDomains();
                // OPTIONAL - only for analysis
                //fadc.saveDomainsWithEntities();
                fadc.printArffPredicatesDataset();
                fadc.printArffTypesDataset();
                fadc.printArffSubjectsDataset();
                fadc.printArffAllDataset();
                break;
            case 6: // all - small domains extension
                DomainExtension de = new DomainExtension();
                ArrayList<String> smallDomains = new ArrayList<>(Arrays.asList(
                    "Royalty_and_nobility", "Mathematics", "Philosophy_and_psychology",
                    "Business.2C_economics.2C_and_finance", "Language_and_linguistics",
                    "Food_and_drink", "Computing"
                ));
                for (String domain : fadc.getBackupDomains()) {
                    if (fadc.domainBackupExists(domain)) {
                        fadc.restoreDomain(domain);
                    }
                    else {
                        System.out.println(domain + " - domain backup does not exist.");
                        Domain newDomain = fadc.addEntities(domain, domain, fadc.getDomainBackupUpperBound(domain), fadc.getDomainBackupLowerBound(domain));
                        System.out.print(newDomain.getName());
                        for (Entity e : newDomain.getEntities()) { // for all entities of domains
                            e.setInformations();
                            System.out.print(".");
                        }
                        System.out.println(".");
                        fadc.backupDomain(newDomain);
                    }
                }
                for (Domain d : fadc.getDomains()) { 
                    if (smallDomains.contains(d.getName())) {
                        System.out.print("Extending domain "+d.getName());
                        de.addSimilarEntitiesByTypes(d);
                        System.out.println(".");
                        fadc.backupDomain(d);
                    }
                    d.analyze();
                }
                fadc.backupDomains();
                fadc.printArffPredicatesDataset();
                fadc.printArffTypesDataset();
                fadc.printArffSubjectsDataset();
                fadc.printArffAllDataset();
                break;
            default:
                System.out.println("unsupported type of informations");
                break;
        }
    }
}
