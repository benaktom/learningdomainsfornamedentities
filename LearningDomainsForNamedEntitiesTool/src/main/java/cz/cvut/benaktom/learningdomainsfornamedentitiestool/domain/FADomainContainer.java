package cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * FADomainContainer extends DomainContainer and process adding Featured Articles
 * entities from wikipedia webpages to domain container
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class FADomainContainer extends DomainContainer {

    public FADomainContainer() {
        super();
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file 'config.properties' not found in the classpath");
            }
        } catch (IOException ex) {
            Logger.getLogger(FADomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        directory = prop.getProperty("FADirectory");
        backupPrefix = prop.getProperty("backupFADomainPrefix");
        backupAllDomains = prop.getProperty("backupFAAllDomainNames");
        backupDomainsBounds = prop.getProperty("backupFADomainBounds");
        arffDir = prop.getProperty("ArffFilesDir");
        arffPredicates = prop.getProperty("ArffFAPredicates");
        arffTypes = prop.getProperty("ArffFATypes");
        arffSubjects = prop.getProperty("ArffFASubjects");
    }
    
    /**
     * Adding entities with upper and lower bounds.
     * This method was created after "long tail" analyzation.
     */
    public void addFAEntities() {
        addEntities("Art.2C_architecture.2C_and_archaeology", "Art.2C_architecture.2C_and_archaeology", 0.4, 0.1);
        addEntities("Biology", "Biology", 0.75, 0.19);
        addEntities("Business.2C_economics.2C_and_finance", "Business.2C_economics.2C_and_finance", 0.31, 0.06);
        addEntities("Chemistry_and_mineralogy", "Chemistry_and_mineralogy", 0.54, 0.07);
        addEntities("Computing", "Computing", 0.57, 0.17);
        addEntities("Culture_and_society", "Culture_and_society", 0.55, 0.16);
        addEntities("Education", "Education", 0.51, 0.15);
        addEntities("Engineering_and_technology", "Engineering_and_technology", 0.5, 0.1);
        addEntities("Food_and_drink", "Food_and_drink", 0.29, 0.1);
        addEntities("Geography_and_places", "Geography_and_places", 0.61, 0.2);
        addEntities("Geology_and_geophysics", "Geology_and_geophysics", 0.4, 0.08);
        addEntities("Health_and_medicine", "Health_and_medicine", 0.53, 0.13);
        addEntities("Heraldry.2C_honors.2C_and_vexillology", "Heraldry.2C_honors.2C_and_vexillology", 0.42, 0.15);
        addEntities("History", "History", 0.28, 0.07);
        addEntities("Language_and_linguistics", "Language_and_linguistics", 0.47, 0.2);
        addEntities("Law", "Law", 0.17, 0.09);
        addEntities("Literature_and_theatre", "Literature_and_theatre", 0.37, 0.12);
        addEntities("Mathematics", "Mathematics", 0.34, 0.1);
        addEntities("Media", "Media", 0.47, 0.16);
        addEntities("Meteorology", "Meteorology", 0.89, 0.12);
        addEntities("Music", "Music", 0.69, 0.25);
        addEntities("Philosophy_and_psychology", "Philosophy_and_psychology", 0.34, 0.16);
        addEntities("Physics_and_astronomy", "Physics_and_astronomy", 0.22, 0.1);
        addEntities("Politics_and_government", "Politics_and_government", 0.22, 0.08);
        addEntities("Religion.2C_mysticism_and_mythology", "Religion.2C_mysticism_and_mythology", 0.2, 0.06);
        addEntities("Royalty_and_nobility", "Royalty_and_nobility", 0.15, 0.0);
        addEntities("Sport_and_recreation", "Sport_and_recreation", 0.54, 0.15);
        addEntities("Transport", "Transport", 0.47, 0.1);
        addEntities("Video_gaming", "Video_gaming", 0.83, 0.2);
        addEntities("Warfare", "Warfare", 0.54, 0.08);
    }

    /**
     * @param domain
     * @param anchor web anchor
     * @param upper bound
     * @param lower bound
     * @return newDomain
     */
    public Domain addEntities(String domain, String anchor, Double upper, Double lower) {
        Domain newDomain = new Domain(domain);
        try {
            Document doc = Jsoup.connect("https://en.wikipedia.org/wiki/Wikipedia:Featured_articles").get();
            Elements elements = doc.select("h2:has(span[id="+anchor+"]) + p a");
            
            for (Element e : elements) {
                String link = e.attr("href").split("/")[2];
                link = "http://dbpedia.org/resource/"+link;
                newDomain.addEntity(new Entity(link));
            }
            if (upper != null && !upper.isNaN()) { newDomain.setPredicateUpperBound(upper); }
            if (lower != null && !lower.isNaN()) { newDomain.setPredicateLowerBound(lower); }
            super.addDomain(newDomain);
            
        } catch (IOException ex) {
            Logger.getLogger(FADomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newDomain;
    }
    
}
