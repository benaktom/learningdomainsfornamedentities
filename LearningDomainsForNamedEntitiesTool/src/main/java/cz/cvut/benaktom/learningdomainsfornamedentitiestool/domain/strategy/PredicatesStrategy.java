package cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.Domain;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Implementation of InformationStrategy for predicates
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class PredicatesStrategy implements InformationStrategy {
    
    /**
     * @param domain 
     */
    @Override
    public void analyzeInformations(Domain domain) {
        System.out.print("Analyzing predicates for "+domain.getName()+"...");
        HashMap res;
        int entitesCount = 0;
        res = new HashMap<String, Integer>();
        for (Entity e : domain.getEntities()) {
            for (String s : e.getPredicates()) {
                if (res.containsKey(s)) {
                    Integer i = (Integer)res.get(s);
                    res.replace(s, i+1);
                }
                else {
                    res.put(s, 1);
                }
            }
            entitesCount++;
        }
        double perc;
        Iterator<String> it = res.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            Object val = res.get(key);
            perc = (int)val / (double)entitesCount;
            /// ATTENTION! upper and lower bound trimming
            if (perc <= domain.getPredicateUpperBound() && perc >= domain.getPredicateLowerBound()) {
                domain.addAnalyzedPredicate(new Domain.MyInfo(key, perc));
                //System.out.println(key + " " + perc * 100 + "%");
            }
        }
        System.out.print("ok\n");
    }

}
