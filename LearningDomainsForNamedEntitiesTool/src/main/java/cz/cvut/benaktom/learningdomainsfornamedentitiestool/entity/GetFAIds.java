package cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.LearningDomainsForNamedEntitiesTool;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Helper class for getting Featured Articles domain ids
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class GetFAIds {
    private ArrayList<String> ids;

    /**
     * Constructor.
     */
    public GetFAIds() {
        ids = new ArrayList<>();
        try {
            Document doc = Jsoup.connect("https://en.wikipedia.org/wiki/Wikipedia:Featured_articles").get();
            Elements elements = doc.select("#mw-content-text > table > tbody > tr:eq(1) p > a");
            for (Element e : elements) {
                String link = e.attr("href");
                ids.add(link);
            }
        } catch (IOException ex) {
            Logger.getLogger(LearningDomainsForNamedEntitiesTool.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<String> getIds() {
        return ids;
    }
    
}
