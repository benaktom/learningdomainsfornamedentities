package cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.Domain;

/**
 * Interface for analyzinig domain informations
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public interface InformationStrategy {
    public void analyzeInformations(Domain domain);
}
