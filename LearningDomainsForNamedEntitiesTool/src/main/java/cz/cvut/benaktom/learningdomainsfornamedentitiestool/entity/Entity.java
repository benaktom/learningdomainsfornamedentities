package cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy.EntityPredicatesStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy.EntitySubjectsStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy.EntityTypesStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy.EntityInformationStrategy;
import java.util.ArrayList;
import org.json.simple.JSONArray;

/**
 * Entity
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class Entity {
    private String url;
    private ArrayList<String> predicates;
    private ArrayList<String> types; // rdf:type
    private ArrayList<String> subjects; // dcterms:subject

    public Entity(String u) {
        url = u;
        predicates = new ArrayList<>();
        types = new ArrayList<>();
        subjects = new ArrayList<>();
    }

    public void setInformations() {
        this.setInformations(new EntityPredicatesStrategy());
        this.setInformations(new EntityTypesStrategy());
        this.setInformations(new EntitySubjectsStrategy());
    }
    
    public void setInformations(EntityInformationStrategy factory) {
        factory.setInformations(this);
    }
    
    public Entity(String u, ArrayList p) {
        url = u;
        predicates = p;
    }
    
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public void setPredicates(JSONArray preds) {
        predicates = new ArrayList<>(preds);
    }
    
    public void addPredicate(String p) {
        predicates.add(p);
    }
    
    /**
     * @return the predicates
     */
    public ArrayList<String> getPredicates() {
        return predicates;
    }

    public void setTypes(JSONArray typs) {
        types = new ArrayList<>(typs);
    }

    public void addType(String t) {
        types.add(t);
    }
    
    /**
     * @return the types
     */
    public ArrayList<String> getTypes() {
        return types;
    }

    public void setSubjects(JSONArray subjs) {
        subjects = new ArrayList<>(subjs);
    }

    public void addSubject(String s) {
        subjects.add(s);
    }
    
    /**
     * @return the subjects
     */
    public ArrayList<String> getSubjects() {
        return subjects;
    }

}
