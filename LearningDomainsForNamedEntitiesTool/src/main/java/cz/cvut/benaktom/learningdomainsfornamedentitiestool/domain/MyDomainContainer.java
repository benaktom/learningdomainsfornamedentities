package cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MyDomainContainer extends DomainContainer and proccess adding three "hand-made"
 * domains
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class MyDomainContainer extends DomainContainer {
    
    public MyDomainContainer() {
        super();
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file 'config.properties' not found in the classpath");
            }
        } catch (IOException ex) {
            Logger.getLogger(MyDomainContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        directory = prop.getProperty("MyDirectory");
        backupPrefix = prop.getProperty("backupMyDomainPrefix");
        backupAllDomains = prop.getProperty("backupMyAllDomainNames");
        backupDomainsBounds = prop.getProperty("backupMyDomainBounds");
        arffDir = prop.getProperty("ArffFilesDir");
        arffPredicates = prop.getProperty("ArffMyPredicates");
        arffTypes = prop.getProperty("ArffMyTypes");
        arffSubjects = prop.getProperty("ArffMySubjects");
    }
    
    public void addMyEntities() {
        Domain sport = new Domain("sport");
        sport.addEntity(new Entity("http://dbpedia.org/resource/Sport"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Ice_hockey"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Olympic_Games"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Diego_Maradona"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Winter_sport"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Football"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Jarom%C3%ADr_J%C3%A1gr"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Jogging"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Swimming"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Athlete"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Inline_skating"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Basketball"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Volleyball"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/World_championship"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Running"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Skiing"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Roger_Federer"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Formula_One"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Ond%C5%99ej_Moravec"));
        sport.addEntity(new Entity("http://dbpedia.org/resource/Kate%C5%99ina_Emmons"));
        sport.setPredicateUpperBound(0.54);
        sport.setPredicateLowerBound(0.15);
        super.addDomain(sport);

        Domain food = new Domain("food");
        food.addEntity(new Entity("http://dbpedia.org/resource/Food"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Restaurant"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Pub"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Agriculture"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Food_technology"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Drink"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Tobacco"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Koffee"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Cook"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Field_(agriculture)"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Harvester"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Beer"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Fish_and_chips"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Kitchen"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Animal_slaughter"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Pizza"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Wheat"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Wine"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Cooker"));
        food.addEntity(new Entity("http://dbpedia.org/resource/Spaghetti"));
        food.setPredicateUpperBound(0.25);
        food.setPredicateLowerBound(0.10);
        super.addDomain(food);

        Domain music = new Domain("music");
        music.addEntity(new Entity("http://dbpedia.org/resource/Music"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Opera"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Musical_instrument"));
        music.addEntity(new Entity("http://dbpedia.org/resource/The_Rolling_Stones"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Karel_Gott"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Music_genre"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Popular_music"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Song"));
        music.addEntity(new Entity("http://dbpedia.org/resource/The_Beatles"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Album"));
        music.addEntity(new Entity("http://dbpedia.org/resource/LP_record"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Concert"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Musician"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Wolfgang_Amadeus_Mozart"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Piano"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Guitar"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Pop_rock"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Punk"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Singing"));
        music.addEntity(new Entity("http://dbpedia.org/resource/Voice_type"));
        music.setPredicateUpperBound(0.65);
        music.setPredicateLowerBound(0.25);
        super.addDomain(music);
    }
    
}
