package cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy.PredicatesStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy.SubjectsStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy.TypesStrategy;
import cz.cvut.benaktom.learningdomainsfornamedentitiestool.domain.strategy.InformationStrategy;
import java.util.ArrayList;

/**
 * Domain contains Entities and analyze its informations
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class Domain {

    /**
     * Struct for analyzed informations.
     */
    public static class MyInfo {
        public String name;
        public double percent;
        public MyInfo(String n, double p) {
            name = n;
            percent = p;
        }
    }
    
    private final String name;
    private final ArrayList<Entity> entities;
    private final ArrayList<MyInfo> analyzedPredicates;
    private final ArrayList<MyInfo> analyzedTypes;
    private final ArrayList<MyInfo> analyzedSubjects;
    private double predicateUpperBound;
    private double predicateLowerBound;
    
    /**
     * Constructor.
     * @param n name
     */
    public Domain(String n) {
        name = n;
        entities = new ArrayList<>();
        analyzedPredicates = new ArrayList<>();
        analyzedTypes = new ArrayList<>();
        analyzedSubjects = new ArrayList<>();
        predicateUpperBound = 1.0;
        predicateLowerBound = 0.0;
    }
    
    /**
     * Analyzing informations for all informations.
     */
    public void analyze() {
        this.analyze(new PredicatesStrategy());
        this.analyze(new TypesStrategy());
        this.analyze(new SubjectsStrategy());
    }
    
    /**
     * Analyzing informations by given InformationStrategy.
     * @param factory 
     */
    public void analyze(InformationStrategy factory) {
        factory.analyzeInformations(this);
    }
    
    /**
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @param e Entity
     */
    public void addEntity(Entity e) {
        entities.add(e);
    }
    
    /**
     * @return entities
     */
    public ArrayList<Entity> getEntities() {
        return entities;
    }
    
    public ArrayList<String> getEntitiesUrls() {
        ArrayList<String> res = new ArrayList<>();
        for (Entity e : entities) { res.add(e.getUrl()); }
        return res;
    }
    
    /**
     * @param ap analyzed predicate
     */
    public void addAnalyzedPredicate(MyInfo ap) {
        analyzedPredicates.add(ap);
    }
    
    /**
     * @return List of analyzed predicates
     */
    public ArrayList<MyInfo> getAnalyzedPredicates() {
        return analyzedPredicates;
    }

    /**
     * @param at analyzed rdf:type
     */
    public void addAnalyzedTypes(MyInfo at) {
        analyzedTypes.add(at);
    }
    
    /**
     * @return List of analyzed rdf:types
     */
    public ArrayList<MyInfo> getAnalyzedTypes() {
        return analyzedTypes;
    }

    /**
     * @param as analyzed dcterms:subject
     */
    public void addAnalyzedSubjects(MyInfo as) {
        analyzedSubjects.add(as);
    }
    
    /**
     * @return List of analyzed dcterms:subjects
     */
    public ArrayList<MyInfo> getAnalyzedSubjects() {
        return analyzedSubjects;
    }

    /**
     * @return the predicateUpperBound
     */
    public double getPredicateUpperBound() {
        return predicateUpperBound;
    }

    /**
     * @param predicateUpperBound the predicateUpperBound to set
     */
    public void setPredicateUpperBound(double predicateUpperBound) {
        this.predicateUpperBound = predicateUpperBound;
    }

    /**
     * @return the predicateLowerBound
     */
    public double getPredicateLowerBound() {
        return predicateLowerBound;
    }

    /**
     * @param predicateLowerBound the predicateLowerBound to set
     */
    public void setPredicateLowerBound(double predicateLowerBound) {
        this.predicateLowerBound = predicateLowerBound;
    }
}
