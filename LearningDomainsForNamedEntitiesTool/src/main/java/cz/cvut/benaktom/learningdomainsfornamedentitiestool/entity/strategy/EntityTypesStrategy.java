package cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;

/**
 * Implementation of EntityInformationStrategy for rdf:types
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class EntityTypesStrategy implements EntityInformationStrategy {

    private final String pathToHDT;
    
    public EntityTypesStrategy() {
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file 'config.properties' not found in the classpath");
            }
        } catch (IOException ex) {
            Logger.getLogger(EntitySubjectsStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        pathToHDT = prop.getProperty("HDT");
    }

    /**
     * @param entity
     * @throws IOException 
     */
    @Override
    public void setInformations(Entity entity) {
        try {
            /* TODO config.properties */
            HDT hdt = HDTManager.mapIndexedHDT(pathToHDT, null);
            
            // Create Jena Model on top of HDT.
            HDTGraph graph = new HDTGraph(hdt);
            Model model = ModelFactory.createModelForGraph(graph);
            
            String queryString1 = "SELECT DISTINCT ?object " +
                    "where { " +
                    "<"+entity.getUrl()+"> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?object . " +
                    "}";
            String queryString2 = "SELECT DISTINCT ?subject " +
                    "where { " +
                    "?subject <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"+entity.getUrl()+"> . " +
                    "}";
            
            QueryExecution qexec = QueryExecutionFactory.create(queryString1, model) ;
            try {
                ResultSet rs = qexec.execSelect() ;
                while (rs.hasNext()) {
                    QuerySolution rb = rs.nextSolution() ;
                    RDFNode x = rb.get("object");
                    if (x.toString().contains("http://dbpedia.org/ontology/")) {
                        entity.addType(" "+x.toString().replace("'", "\\'"));
                    }
                }
            }
            finally {
                qexec.close() ;
            }
            qexec = QueryExecutionFactory.create(queryString2, model) ;
            try {
                ResultSet rs = qexec.execSelect() ;
                while (rs.hasNext()) {
                    QuerySolution rb = rs.nextSolution() ;
                    RDFNode x = rb.get("subject") ;
                    if (x.toString().contains("http://dbpedia.org/ontology/")) {
                        entity.addType("is "+x.toString().replace("'", "\\'"));
                    }
                }
            }
            finally {
                qexec.close() ;
            }
        }
        catch (IOException ex) {
            Logger.getLogger(EntityTypesStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
