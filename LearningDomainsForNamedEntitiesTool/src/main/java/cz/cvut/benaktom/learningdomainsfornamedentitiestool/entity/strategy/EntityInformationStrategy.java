package cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.strategy;

import cz.cvut.benaktom.learningdomainsfornamedentitiestool.entity.Entity;

/**
 * Interface for setting entity informations
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public interface EntityInformationStrategy {
    public void setInformations(Entity entity);
}
