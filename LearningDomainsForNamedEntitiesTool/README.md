# Learning Domains for Named Entities #

## The TOOL of the Learning Domains for Named Entities Project ##

The TOOL reads [Featured Articles](https://en.wikipedia.org/wiki/Wikipedia:Featured_articles) entities and manually found entities, it analyzes its predicates, rdf:types and dcterms:subjects by DBpedia dataset (as HDT file) and then it exports ARFF files for further processing. 

### Requirements ###

 * JDK 8
 * Apache Maven
 * GIT
 * Downloaded DBpedia .hdt and .hdt.index files, eg. from [here](http://users.restdesc.org/rgverbor/tmp/hdt/)

### Configuration ###

In the file **src/main/resources/config.properties** set path to your DBpedia.hdt file. Other properties in this file may not be changed.

Be sure to match of setting the correct directory.

### Arguments ###

The TOOL works with two arguments:
 
the **first argument** determines which entities are used

* 1 - manually found entities in the three domain
* 2 - Featured Articles entities
* 3 - support method for analyze percentage representation of predicates of Featured Articles entities (second argument can be 0)

the **second argument** determines which informations are processed

* 1 - only predicates (without backup\*)
* 2 - only rdf:types (without backup\*)
* 3 - only dcterms:subjects (without backup\*)
* 4 - all informations
  - manually entities - backup\* and print arff files
  - Featured Articles entities - only backup\*
* 5 - only for Featured Articles entities - restore from backup* and print arff files
* 6 - only for Featured Articles entities - same as 5 above that, extending the small domains (with a small number of entities)

**recommended configuration of arguments**: 2 5

### Annotations ###

**backup**\* - the helper function which saves Domain and Entity objects to JSON files


### Installation instructions ###

```sh
$ mkdir LearningDomainsForNamedEntities
$ cd LearningDomainsForNamedEntities/
$ git init
$ git pull https://benaktom@bitbucket.org/benaktom/learningdomainsfornamedentities.git
$ cd LearningDomainsForNamedEntitiesTool/
$ mvn clean
$ mvn package
$ mvn exec:java -Dexec.args="2 5"
```

### License ###

Licensed under the [GNU General Public License Version 3 (GNU GPLv3)](http://www.gnu.org/licenses/gpl.html).

Copyright (c) 2016 Tomáš Benák (<benaktom@fit.cvut.cz>)

