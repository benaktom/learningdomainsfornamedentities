# Learning Domains for Named Entities #

The project, stands on DBpedia (RDF) dataset, deals with learning domains for named entities based on its predicates, rdf:types and dcterms:subjects. The Project is divided into two parts, [TOOL](LearningDomainsForNamedEntitiesTool) and [REST API](LearningDomainsForNamedEntitiesApi) and it is part of the author's master's thesis.

## Author ##

[Tomáš Benák](mailto:benaktom@fit.cvut.cz)

## Supervisor ##

[Milan Dojchinovski](mailto:milan.dojchinovski@fit.cvut.cz)

## License ##

Licensed under the [GNU General Public License Version 3 (GNU GPLv3)](http://www.gnu.org/licenses/gpl.html).

Copyright (c) 2016 Tomáš Benák (<benaktom@fit.cvut.cz>)
