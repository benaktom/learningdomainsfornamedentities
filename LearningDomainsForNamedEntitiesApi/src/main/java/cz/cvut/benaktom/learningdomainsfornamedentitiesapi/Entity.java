package cz.cvut.benaktom.learningdomainsfornamedentitiesapi;

import java.io.IOException;
import java.util.ArrayList;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
public class Entity {
    /* change path to your HDT file here */
    //private static final String PATH_TO_HDT_FILE = "/home/tomas/FIT/MI/DIP/dbpedia2015en.hdt";
    
    private final String domain;
    private final String url;
    private final ArrayList<String> predicates;
    private final ArrayList<String> types; // rdf:type
    private final ArrayList<String> subjects; // dcterms:subject
    private HDT hdt;
    private final HDTGraph graph;
    private final Model model;

    /**
     * Entity constructor
     * @param d (domain)
     * @param u (url)
     * @throws IOException 
     */
    public Entity(String d, String u, String pathToHDTFile) {
        domain = d;
        url = u;
        predicates = new ArrayList<>();
        types = new ArrayList<>();
        subjects = new ArrayList<>();
        try {
            //hdt = HDTManager.mapIndexedHDT(PATH_TO_HDT_FILE, null);
            hdt = HDTManager.mapIndexedHDT(pathToHDTFile, null);
        } catch (IOException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
        graph = new HDTGraph(hdt);
        model = ModelFactory.createModelForGraph(graph);
    }
    
    /**
     * Setting predicates using HDT Jena.
     */
    public void setPredicates() {
        String queryString1 = "SELECT DISTINCT ?predicate " +
                "where { " + 
                "<"+url+"> ?predicate ?object . " +
                "}";
        String queryString2 = "SELECT DISTINCT ?predicate " +
                "where { " + 
                "?subject ?predicate <"+url+"> . " +
                "}";
        
        QueryExecution qexec = QueryExecutionFactory.create(queryString1, model) ;
        try {
            ResultSet rs = qexec.execSelect() ;
            while (rs.hasNext()) {
                QuerySolution rb = rs.nextSolution() ;
                RDFNode x = rb.get("predicate") ;
                predicates.add(" "+x.toString());
            }
        }
        finally {
            qexec.close() ;
        }
        qexec = QueryExecutionFactory.create(queryString2, model) ;
        try {
            ResultSet rs = qexec.execSelect() ;
            while (rs.hasNext()) {
                QuerySolution rb = rs.nextSolution() ;
                RDFNode x = rb.get("predicate") ;
                predicates.add("is "+x.toString());
            }
        }
        finally {
            qexec.close() ;
        }
    }
    
    /**
     * Setting rdf:types using HDT Jena.
     */
    public void setTypes() {
        String queryString1 = "SELECT DISTINCT ?object " +
                "where { " + 
                "<"+url+"> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?object . " +
                "}";
        String queryString2 = "SELECT DISTINCT ?subject " +
                "where { " + 
                "?subject <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"+url+"> . " +
                "}";
        
        QueryExecution qexec = QueryExecutionFactory.create(queryString1, model) ;
        try {
            ResultSet rs = qexec.execSelect() ;
            while (rs.hasNext()) {
                QuerySolution rb = rs.nextSolution() ;
                RDFNode x = rb.get("object");
                if (x.toString().contains("http://dbpedia.org/ontology/")) {
                    types.add(" "+x.toString().replace("'", "\\'"));
                }
            }
        }
        finally {
            qexec.close() ;
        }
        qexec = QueryExecutionFactory.create(queryString2, model) ;
        try {
            ResultSet rs = qexec.execSelect() ;
            while (rs.hasNext()) {
                QuerySolution rb = rs.nextSolution() ;
                RDFNode x = rb.get("subject") ;
                if (x.toString().contains("http://dbpedia.org/ontology/")) {
                    types.add("is "+x.toString().replace("'", "\\'"));
                }
            }
        }
        finally {
            qexec.close() ;
        }
    }
 
    /**
     * Setting dcterms:subject using HDT Jena.
     */
    public void setSubjects() {
        String queryString1 = "SELECT DISTINCT ?object " +
                "where { " + 
                "<"+url+"> <http://purl.org/dc/terms/subject> ?object . " +
                "}";
        String queryString2 = "SELECT DISTINCT ?subject " +
                "where { " + 
                "?subject <http://purl.org/dc/terms/subject> <"+url+"> . " +
                "}";
        
        QueryExecution qexec = QueryExecutionFactory.create(queryString1, model) ;
        try {
            ResultSet rs = qexec.execSelect() ;
            while (rs.hasNext()) {
                QuerySolution rb = rs.nextSolution() ;
                RDFNode x = rb.get("object");
                subjects.add(" "+x.toString().replace("'", "\\'"));
            }
        }
        finally {
            qexec.close() ;
        }
        qexec = QueryExecutionFactory.create(queryString2, model) ;
        try {
            ResultSet rs = qexec.execSelect() ;
            while (rs.hasNext()) {
                QuerySolution rb = rs.nextSolution() ;
                RDFNode x = rb.get("subject") ;
                subjects.add("is "+x.toString().replace("'", "\\'"));
            }
        }
        finally {
            qexec.close() ;
        }
    }
    
    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the predicates
     */
    public ArrayList<String> getPredicates() {
        return predicates;
    }

    /**
     * @return the types
     */
    public ArrayList<String> getTypes() {
        return types;
    }

    /**
     * @return the subjects
     */
    public ArrayList<String> getSubjects() {
        return subjects;
    }

}
