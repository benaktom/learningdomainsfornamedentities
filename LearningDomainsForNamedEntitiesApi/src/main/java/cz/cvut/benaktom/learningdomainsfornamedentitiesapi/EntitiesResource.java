package cz.cvut.benaktom.learningdomainsfornamedentitiesapi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import org.json.simple.JSONObject;

/**
 *
 * @author Tomáš Benák <benaktom@fit.cvut.cz>
 */
@Path("{entity}/domains")
@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
public class EntitiesResource {
    private Entity testedEntity;
    private String sourceDir;
    private String classifier;
    
    /**
     * Initialization of testedEntity and path to arff and model files
     * @param entityId
     */
    protected void init(String entityId) {
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file 'config.properties' not found in the classpath");
            }
        } catch (IOException ex) {
            Logger.getLogger(EntitiesResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        String pathToHDTFile = prop.getProperty("pathToHDTFile");
        
        testedEntity = new Entity("", "http://dbpedia.org/resource/"+entityId, pathToHDTFile);
        testedEntity.setPredicates();
        testedEntity.setTypes();
        testedEntity.setSubjects();
        
        // path to arff and model files from src/main/resources
        sourceDir = prop.getProperty("sourceDir");
        classifier = prop.getProperty("classifier");
    }
    
    /**
     * Getting domain for testedEntity.
     * Content-type: text/plain
     * @param entityId
     * @return String with informations about domain
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getDomainAsPlainText(@PathParam("entity") String entityId) {
        init(entityId);
        
        String res = "";
        res += "Entity: "+testedEntity.getUrl()+"\n";
        String[] res1 = getPredicitonForPredicates(testedEntity);
        res += "predicates: "+res1[0]+" "+res1[1]+"%\n";
        String[] res2 = getPredicitonForTypes(testedEntity);
        res += "rdf:types: "+res2[0]+" "+res2[1]+"%\n";
        String[] res3 = getPredicitonForSubjects(testedEntity);
        res += "dcterms:subjects: "+res3[0]+" "+res3[1]+"%\n";
        // only for fa_all_domains
        if (sourceDir.contains("fa_all_domains")) {
            String[] res4 = getPredicitonForAllInfo(testedEntity);
            res += "all_info: "+res4[0]+" "+res4[1]+"%\n";
        }
        return res;
    }
    
    /**
     * Getting domain for testedEntity.
     * Content-type: application/json
     * @param entityId
     * @return JSON String with informations about domain
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getDomainAsJson(@PathParam("entity") String entityId) {
        init(entityId);
        
        JSONObject json = new JSONObject();
        json.put("url", testedEntity.getUrl());
        
        String[] res1 = getPredicitonForPredicates(testedEntity);
        JSONObject preds = new JSONObject();
        preds.put("domain", res1[0]);
        preds.put("percents", res1[1]);
        json.put("predicates", preds);
        String[] res2 = getPredicitonForTypes(testedEntity);
        JSONObject types = new JSONObject();
        types.put("domain", res2[0]);
        types.put("percents", res2[1]);
        json.put("rdf:types", types);
        String[] res3 = getPredicitonForSubjects(testedEntity);
        JSONObject subjs = new JSONObject();
        subjs.put("domain", res3[0]);
        subjs.put("percents", res3[1]);
        json.put("dcterms:subjects", subjs);
        // only for fa_all_domains
        if (sourceDir.contains("fa_all_domains")) {
            String[] res4 = getPredicitonForAllInfo(testedEntity);
            JSONObject allInfo = new JSONObject();
            allInfo.put("domain", res4[0]);
            allInfo.put("percents", res4[1]);
            json.put("all_info", allInfo);
        }
        
        return json.toJSONString();
    }

    /**
     * Reads arff file and getting prediction for predicates from model.
     * @param testedEntity
     * @param arffFile
     * @param modelFile
     * @return Array of String about predicted domain
     */
    private String[] getPredicitonForPredicates(Entity testedEntity) {
        String[] res = new String[5];
        
        try {
            File arff = new File(getClass().getClassLoader().getResource(sourceDir+"/predicates.arff").getFile());
            Instances data;
            try (BufferedReader reader = new BufferedReader(new FileReader(arff.getAbsolutePath()))) {
                data = new Instances(reader);
            }
            Enumeration<Attribute> e = data.enumerateAttributes();

            FastVector attributes = new FastVector(data.numAttributes());
            while (e.hasMoreElements()) {
                Attribute knownAttr = e.nextElement();
                attributes.addElement(knownAttr.copy());
            }
            Instances evalData = new Instances("MyRelation", attributes, 0);
            evalData.setClassIndex(evalData.numAttributes() - 1);

            e = data.enumerateAttributes();
            Instance i = new Instance(data.numAttributes());
            boolean equals;
            int k = 0;
            while (e.hasMoreElements()) {
                Attribute attr = e.nextElement();
                equals = false;
                if (testedEntity.getPredicates().contains(attr.name())) {
                    i.setValue(evalData.attribute(k), "t");
                    equals = true;
                }
                if (equals == false && !attr.name().equals("class")) {
                    i.setValue(evalData.attribute(k),  "f");
                }
                k++;
            }
            evalData.add(i);
            
            File model = new File(getClass().getClassLoader().getResource(sourceDir+"/"+classifier+"/predicates.model").getFile());
            Classifier classifier = (Classifier) weka.core.SerializationHelper.read(model.getAbsolutePath());
            int classIndex = (int) classifier.classifyInstance(evalData.instance(0));
            String classLabel = evalData.firstInstance().classAttribute().value(classIndex);
            double pred[] = classifier.distributionForInstance(evalData.instance(0));
            double probability = pred[classIndex];
            res[0] = classLabel;
            res[1] = String.format("%.2f", probability * 100);
        } catch (Exception ex) {
            Logger.getLogger(EntitiesResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    /**
     * 
     * Reads arff file and getting prediction for rdf:types from model.
     * @param testedEntity
     * @param arffFile
     * @param modelFile
     * @return Array of String about predicted domain
     */
    private String[] getPredicitonForTypes(Entity testedEntity) {
        String[] res = new String[2];
        
        try {
            File arff = new File(getClass().getClassLoader().getResource(sourceDir+"/types.arff").getFile());
            Instances data;
            try (BufferedReader reader = new BufferedReader(new FileReader(arff.getAbsolutePath()))) {
                data = new Instances(reader);
            }
            Enumeration<Attribute> e = data.enumerateAttributes();

            FastVector attributes = new FastVector(data.numAttributes());
            while (e.hasMoreElements()) {
                Attribute knownAttr = e.nextElement();
                attributes.addElement(knownAttr.copy());
            }
            Instances evalData = new Instances("MyRelation", attributes, 0);
            evalData.setClassIndex(evalData.numAttributes() - 1);
            
            e = data.enumerateAttributes();
            Instance i = new Instance(data.numAttributes());
            boolean equals;
            int k = 0;
            while (e.hasMoreElements()) {
                Attribute attr = e.nextElement();
                equals = false;
                if (testedEntity.getTypes().contains(attr.name())) {
                    i.setValue(evalData.attribute(k), "t");
                    equals = true;
                }
                if (equals == false && !attr.name().equals("class")) {
                    i.setValue(evalData.attribute(k), "f");
                }
                k++;
            }
            evalData.add(i);

            File model = new File(getClass().getClassLoader().getResource(sourceDir+"/"+classifier+"/types.model").getFile());
            Classifier classifier = (Classifier) weka.core.SerializationHelper.read(model.getAbsolutePath());
            int classIndex = (int) classifier.classifyInstance(evalData.instance(0));
            String classLabel = evalData.firstInstance().classAttribute().value(classIndex);
            double pred[] = classifier.distributionForInstance(evalData.instance(0));
            double probability = pred[classIndex];
            res[0] = classLabel;
            res[1] = String.format("%.2f", probability * 100);
        } catch (Exception ex) {
            Logger.getLogger(EntitiesResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    /**
     * Reads arff file and getting prediction for dcterms:subjects from model.
     * @param testedEntity
     * @param arffFile
     * @param modelFile
     * @return Array of String about predicted domain
     */
    private String[] getPredicitonForSubjects(Entity testedEntity) {
        String[] res = new String[2];
        
        try {
            File arff = new File(getClass().getClassLoader().getResource(sourceDir+"/subjects.arff").getFile());
            Instances data;
            try (BufferedReader reader = new BufferedReader(new FileReader(arff.getAbsolutePath()))) {
                data = new Instances(reader);
            }
            Enumeration<Attribute> e = data.enumerateAttributes();

            FastVector attributes = new FastVector(data.numAttributes());
            while (e.hasMoreElements()) {
                Attribute knownAttr = e.nextElement();
                attributes.addElement(knownAttr.copy());
            }
            Instances evalData = new Instances("MyRelation", attributes, 0);
            evalData.setClassIndex(evalData.numAttributes() - 1);

            e = data.enumerateAttributes();
            Instance i = new Instance(data.numAttributes());
            boolean equals;
            int k = 0;
            while (e.hasMoreElements()) {
                Attribute attr = e.nextElement();
                equals = false;
                if (testedEntity.getSubjects().contains(attr.name())) {
                    i.setValue(evalData.attribute(k), "t");
                    equals = true;
                }
                if (equals == false && !attr.name().equals("class")) {
                    i.setValue(evalData.attribute(k), "f");
                }
                k++;
            }
            evalData.add(i);

            File model = new File(getClass().getClassLoader().getResource(sourceDir+"/"+classifier+"/subjects.model").getFile());
            Classifier classifier = (Classifier) weka.core.SerializationHelper.read(model.getAbsolutePath());
            int classIndex = (int) classifier.classifyInstance(evalData.instance(0));
            String classLabel = evalData.firstInstance().classAttribute().value(classIndex);
            double pred[] = classifier.distributionForInstance(evalData.instance(0));
            double probability = pred[classIndex];
            res[0] = classLabel;
            res[1] = String.format("%.2f", probability * 100);
        } catch (Exception ex) {
            Logger.getLogger(EntitiesResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    /**
     * Reads arff file and getting prediction for all info from model.
     * @param testedEntity
     * @param arffFile
     * @param modelFile
     * @return Array of String about predicted domain
     */
    private String[] getPredicitonForAllInfo(Entity testedEntity) {
        String[] res = new String[5];
        
        try {
            File arff = new File(getClass().getClassLoader().getResource(sourceDir+"/merged.arff").getFile());
            Instances data;
            try (BufferedReader reader = new BufferedReader(new FileReader(arff.getAbsolutePath()))) {
                data = new Instances(reader);
            }
            Enumeration<Attribute> e = data.enumerateAttributes();

            FastVector attributes = new FastVector(data.numAttributes());
            while (e.hasMoreElements()) {
                Attribute knownAttr = e.nextElement();
                attributes.addElement(knownAttr.copy());
            }
            Instances evalData = new Instances("MyRelation", attributes, 0);
            evalData.setClassIndex(evalData.numAttributes() - 1);

            e = data.enumerateAttributes();
            Instance i = new Instance(data.numAttributes());
            boolean equals;
            int k = 0;
            while (e.hasMoreElements()) {
                Attribute attr = e.nextElement();
                equals = false;
                if (testedEntity.getPredicates().contains(attr.name())
                        || testedEntity.getTypes().contains(attr.name())
                        || testedEntity.getSubjects().contains(attr.name())) {
                    i.setValue(evalData.attribute(k),  "t");
                    equals = true;
                }
                if (equals == false && !attr.name().equals("class")) {
                    i.setValue(evalData.attribute(k),  "f");
                }
                k++;
            }
            evalData.add(i);
            
            File model = new File(getClass().getClassLoader().getResource(sourceDir+"/"+classifier+"/merged.model").getFile());
            Classifier classifier = (Classifier) weka.core.SerializationHelper.read(model.getAbsolutePath());
            int classIndex = (int) classifier.classifyInstance(evalData.instance(0));
            String classLabel = evalData.firstInstance().classAttribute().value(classIndex);
            double pred[] = classifier.distributionForInstance(evalData.instance(0));
            double probability = pred[classIndex];
            res[0] = classLabel;
            res[1] = String.format("%.2f", probability * 100);
        } catch (Exception ex) {
            Logger.getLogger(EntitiesResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }
    
}
